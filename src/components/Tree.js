import { useEffect, useState } from "react"
import './Tree.css';

export const Tree = (props) => {

  const { sourceData } = props

  const [data, setData] = useState(sourceData)
  const [treeElements, setTreeElements] = useState([])
  const [isEditing, setEditState] = useState(false)

  const addChildren = (parentId) => {
    setData((data) => {
      const parent = parentId === "-1" ? data : find(data, parentId)
      const newChildren = { id: `${parentId}-${parent.children.length + 1}`, label: `${parent.label}-${parent.children.length + 1 || 1}`, children: [] }
      parent.children.push(newChildren)
      const elements = render(data, isEditing)
      setTreeElements(elements)
      return { ...data }
    })
  }

  const editNode = (id, label) => {
    setData((data) => {
      const node = find(data, id)
      node.label = label
      const elements = render(data, true)
      setTreeElements(elements)
      return { ...data }
    })
  }

  const removeChildren = (parentId, id) => {
    setData((data) => {
      const parent = parentId === "-1" ? data : find(data, parentId)
      parent.children = parent.children.filter(child => child.id !== id)
      const elements = render(data)
      setTreeElements(elements)
      return { ...data }
    })
  }

  const reset = () => {
    setData((data) => {
      data.children = []
      const elements = render(data)
      setTreeElements(elements)
      return { ...data }
    })
  }

  useEffect(() => {
    const elements = render(data)
    setTreeElements(elements)
  }, [])

  const find = (data, id) => {
    if (data.children.length === 0) {
      return
    }

    for (const item of data.children) {
      if (item.id !== id) {
        const obj = find(item, id)
        if (obj) {
          return obj
        }
      } else {
        return item
      }
    }
  }

  const editNodes = () => {
    setEditState((isEditing) => {
      isEditing = false
      const elements = render(data, isEditing)
      setTreeElements(elements)
      return isEditing
    }
    )
  }

  const showEditForm = () => {
    setEditState((isEditing) => {
      isEditing = true
      const elements = render(data, isEditing)
      setTreeElements(elements)
      return isEditing
    }
    )
  }

  const render = (data, editState) => {
    if (data.children.length === 0) {
      return
    }

    return data.children.map(item => {
      return (
        <>
          <div className="node" key={`${data.label}_${data.id}`}>
            {(editState)
              ? (
                <input
                  type="text"
                  value={item.label}
                  onChange={(e) => {
                    editNode(item.id, e.target.value)
                  }} />)
              : (<label>{item.label}</label>)}
            <div className="nodeButtons">
              <button onClick={() => addChildren(item.id)}>Add child</button>
              <button onClick={() => removeChildren(data.id, item.id)}>Delete</button>
            </div>
          </div>
          <ul className="childrenNode">
            {render(item, editState)}
          </ul>
        </>
      )
    })
  }

  return (
    <>
      <div className="topButtons">
        {(isEditing)
          ? (<button onClick={() => editNodes()}>Save</button>)
          : (<button onClick={() => showEditForm()}>Edit nodes</button>)
        }
        <button onClick={() => reset()}>Reset</button>
        <button onClick={() => addChildren("-1")}>Add root element</button>
      </div>
      <br />
      {treeElements}
    </>
  )

}