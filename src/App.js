import './App.css';
import { Tree } from './components'

function App() {
  return (
    <div className="App">
      <header style={{ backgroundColor: "gray" }}>
        <h1>
          Tree App
        </h1>
      </header>
      <div className='container'>
        <Tree sourceData={data} />
      </div>
    </div>
  );
}

const data = {
  id: "-1",
  label: "root",
  children:
    [
      {
        id: "0",
        label: "Documents",
        children: [
          {
            id: "0-0",
            label: "Document 1-1",
            children: [
              {
                id: "0-1-1",
                label: "Document-0-1",
                children: []
              },
              {
                id: "0-1-2",
                label: "Document-0-2",
                children: []
              },
            ],
          },
        ],
      },
      {
        id: "1",
        label: "Desktop",
        children: [
          {
            id: "1-0",
            label: "document1",
            children: []
          },
          {
            id: "1-1",
            label: "documennt-2",
            children: []
          },
        ],
      },
      {
        id: "2",
        label: "Downloads",
        children: [],
      }
    ]
};

export default App;
